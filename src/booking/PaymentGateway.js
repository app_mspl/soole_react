
import React, { Component } from 'react';
import Rave from 'react-native-rave';

var rideDetail = null
export default class PaymentGateway extends Component {
    constructor(props) {
        super(props);
        this.onSuccess = this.onSuccess.bind(this);
        this.onFailure = this.onFailure.bind(this);
        this.onClose = this.onClose.bind(this);
        rideDetail = this.props.navigation.state.params.paymentDetail
        console.log('payment', rideDetail)
    }

    onSuccess(data) {
        console.log("success", data);
        // You can get the transaction reference from successful transaction charge response returned and handle your transaction verification here

    }

    onFailure(data) {
        console.log("error", data);
    }

    onClose() {
        //navigate to the desired screen on rave close

    }
    render() {
        return (
            <Rave
                amount="500"
                country="NG"
                currency="NGN"
                email="test@mail.com"
                firstname="Oluwole"
                lastname="Adebiyi"
                publickey="FLWPUBK_TEST-eb3412c1a925d32c9a611f3afd1a17e1-X"
                encryptionkey="FLWSECK_TEST0cb64e054c98"
                meta={[{ metaname: "color", metavalue: "red" }, { metaname: "storelocation", metavalue: "ikeja" }]}
                production={false}
                onSuccess={res => this.onSuccess(res)}
                onFailure={e => this.onFailure(e)}
                onClose={e => this.onClose(e)}
            />
        );
    }
}