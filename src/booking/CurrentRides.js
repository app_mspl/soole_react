import React, { Component } from 'react'
import { Text, ActivityIndicator, View, ScrollView, TouchableOpacity, Button } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const apiUrl = 'http://93.188.167.68:3100/api/rides/getRides/'

let isLoading = true;
export default class CurrentRides extends Component {
    constructor() {
        super();
        this.getCurrentRide()
        this.state = {
            userData: []
        }
    }

    getCurrentRide = async () => {
        try {
            let response = await fetch(
                `http://93.188.167.68:3100/api/rides/getRides?id=${global.userId}&status=${'active'}`
            );
            let responseJson = await response.json();
            console.log('apiResponse', responseJson)
            if (responseJson.isSuccess) {
                isLoading = false
                this.setState({
                    userData: responseJson.data
                })
            } else {
                isLoading = true
                alert('Somthing went wrong')
            }
        } catch (error) {
            console.error(error);
        }
    }
    cancelRide = async (data) => {
        var cencelRide = JSON.stringify(data)
        fetch(`http://93.188.167.68:3100/api/rides/cencelRide/${data.id}`, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: cencelRide
        })
            .then(response =>
                response.json()).then(response => {
                    if (response.isSuccess) {
                        isLoading = false
                        alert('Ride Cancel Successfully')
                        this.getCurrentRide()
                    } else {
                        isLoading = true
                        alert('Somthing went wrong')
                    }
                })
            .catch(error => {
                console.log('Error:', error)
                this.setState({
                    animate: false
                })
            })
    }

    renderScreen = (rides, noRide) => {
        return (
            <View style={{ flex: 1, margin: 20 }}>
                <View style={{ backgroundColor: '#636FA4', height: 50, marginTop: 20, marginBottom: 20, borderRadius: 3, justifyContent: 'center', paddingLeft: 20 }}>
                    <Text style={{ color: '#FFF', fontWeight: '500' }}>YOUR OFFERED RIDES</Text>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {noRide == true ? <Text style={{ color: 'black' }}>No Current Ride Available</Text> : null}
                    {
                        rides.map(ride => {
                            return (<View>
                                <View style={Styles.RideView}>
                                    <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                                            Ride At
             </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                                            {ride.rideAt}
                                        </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                                            Pick Up Loaction
                  </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                                            {ride.origin_addresses}
                                        </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                                            Destination
                  </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                                            {ride.destination_addresses}
                                        </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Seater</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{ride.seater}</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{ backgroundColor: '#636FA4', height: 50, borderRadius: 3, justifyContent: 'center', }} onPress={() => this.cancelRide(ride)}>
                                            <Text style={{ color: '#FFF', fontWeight: '500', textAlign: 'center' }}> Cancel Ride </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {/* <View style={{ marginTop: 30 }}>
                                    <Button title="SCHEDULE A RIDE " color="#636FA4" />
                                </View> */}
                            </View>)
                        })
                    }

                </ScrollView>
            </View>
        )


    }
    render() {

        if (isLoading) {
            return (
                <ActivityIndicator
                    animating={true}
                    style={Styles.indicator}
                    size='large'
                />
            );
        } else {
            if (this.state.userData.length) {
                return this.renderScreen(this.state.userData, false)
            }
            else {
                return this.renderScreen([], true)
            }
        }

    }
}

