import React, { Component } from 'react'
import { Alert, Text, View, TextInput, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, Dimensions, BackHandler } from 'react-native'
import Styles from '../../Stylesheet'
import { RNNumberStepper } from 'react-native-number-stepper';
import MapComponent from '../Gmap/GoogleMap';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import HandleBack from '../../HandleBack';
import DatePicker from 'react-native-datepicker-latest'
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
var currentHours = new Date().getHours(); //Current Hours
var currentMinutes = new Date().getMinutes(); //Current Minutes
var currentDate = new Date().getDate(); //Current Date
var currentMonth = new Date().getMonth() + 1; //Current Month
var currentYear = new Date().getFullYear(); //Current Year

export default class FindRide extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rideAt: null,
            seater: "1",
            locationDetail: new Object,
            Amount: 0,
            price: 0,
            email: '',
            paymentMode: "cash"
        };

    }


    componentDidMount() {
        let setMonth = currentMonth <= 9 ? "0" + currentMonth : currentMonth;
        let setDate = currentDate <= 9 ? "0" + currentDate : currentDate;
        let setMinutes = currentMinutes <= 9 ? "0" + currentMinutes : currentMinutes;
        let setHours = currentHours <= 9 ? "0" + currentHours : currentHours;
        this.setState({
            rideAt: setHours + ":" + setMinutes

        });

    }
    onBack = () => {
        Alert.alert(
            "",
            "Do You Want to Quit The App?",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
        return true;
    }

    getLocationDetail = (location) => {
        console.log('location Detail', location)
        this.setState({ locationDetail: location });
        let amount = parseInt(this.state.locationDetail.pickDropLoction.pickUp.price)
        this.setState({
            Amount: amount,
            price: amount
        })
    }
    reserveSpot = () => {
        let reserveSpotDetail = this.state
        if (!this.state.locationDetail.distance) {
            alert('Please Select Destination')

        } else {
            if (this.state.paymentMode === 'cash') {

                this.props.navigation.navigate('AvailableCar', { detail: reserveSpotDetail });
            } else {
                this.props.navigation.navigate('PaymentGateway', { paymentDetail: reserveSpotDetail });
            }

        }
    }
    setAmount = (seater) => {
        console.log("methodCalled")

        let totalAmount = this.state.price * seater
        this.setState({ Amount: totalAmount })
        this.setState({ seater: seater })
    }
    render() {
        var radio_props = [
            { label: 'Cash ', value: "Cash" },
            { label: 'Online', value: "Online" }
        ];

        return (
            <HandleBack onBack={this.onBack} style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={Styles.HeaderRide}>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                            <FontAwesome name="bars" size={30} color="#fff" />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                            <Text style={{ color: '#fff', fontWeight: '500', }}>Book Ride</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={[{ flex: 1, backgroundColor: '#d3d3d3', minHeight: 300 }]}>
                            <MapComponent onSelectLoction={this.getLocationDetail} />
                        </View>
                        <View style={{ flex: 1, margin: 20, }}>
                            <View style={{ flex: 1, backgroundColor: '#fff', elevation: 5, top: -50 }}>
                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                    <View style={{ flex: 1 }} >
                                        <Text style=
                                            {{
                                                color: this.state.RideText,
                                                fontWeight: '500',
                                                textAlign: 'center',
                                            }}>
                                            RIDE
                                            </Text>
                                    </View>
                                </View>

                                <View style={[Styles.FindRideView,]} >
                                    <View style={[Styles.AJ, {
                                        flex: 1, flexDirection: 'row',

                                    }]}>
                                        <Text style={{ fontWeight: '500', fontSize: 20, padding: 5 }}>Ride At</Text>
                                        <Text style={{
                                            fontSize: 20,
                                            // justifyContent: 'center',
                                            // borderWidth: 1,
                                            // borderColor: '#636FA4',
                                            // borderRadius: 8,
                                            // height: 43,
                                            textAlign: 'center',
                                            // width: 155, 
                                            color: '#636FA4',
                                            fontWeight: '500'
                                        }}>{this.state.rideAt}</Text>
                                    </View>


                                    {/* <DatePicker
                                                        style={[{
                                                            // borderWidth: 1,
                                                            // borderColor: '#636FA4',
                                                            // borderRadius: 8,
                                                            // width: 155
                                                        }]}
                                                        date={this.state.rideAt}
                                                        mode="time"
                                                        placeholder="select time"
                                                        confirmBtnText="Confirm"
                                                        cancelBtnText="Cancel"
                                                        customStyles={{
                                                            dateIcon: {
                                                                position: 'absolute',
                                                                left: 0,
                                                                top: 4,
                                                                marginLeft: 0
                                                            },
                                                            dateInput: {
                                                                marginLeft: 36,
                                                                borderColor: 'white',
                                                                borderRadius: 8,
                                                            }
                                                        }}
                                                        onDateChange={(date) => { this.setState({ rideAt: date }) }}
                                                    /> */}

                                </View>

                                <View style={[Styles.FindRideView,]} >
                                    <View style={[Styles.AJ, { flex: 1 }]}>
                                        <Text style={{ fontWeight: '500', marginTop: 10 }}>Seater</Text>
                                        <Text style={{ color: "#636FA4", fontSize: 20, fontWeight: '500' }}>{this.state.locationDetail.distance}</Text>
                                    </View>
                                    <View style={[Styles.AJ, { flex: 1, justifyContent: 'flex-start' }]}>

                                        <View>
                                            <RNNumberStepper
                                                size={1}
                                                value={1}
                                                minValue={1}
                                                maxValue={4}
                                                autoRepeat={false}
                                                buttonsBackgroundColor='#fff'
                                                buttonsTextColor='#000'
                                                labelBackgroundColor='#fff'
                                                labelTextColor='#000'
                                                borderWidth={1}
                                                borderColor='#636FA4'
                                                onChange={(value) => {
                                                    this.setAmount(value)
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={{ margin: 40 }}>
                                    <View >
                                        <Text style={{ fontWeight: '500', marginBottom: 10 }}>Payment Mode</Text>
                                        <RadioForm
                                            radio_props={radio_props}
                                            initial={0}
                                            buttonSize={10}
                                            formHorizontal={true}
                                            buttonColor={'#636FA4'}
                                            // labelColor={'#636FA4'}
                                            // labelWrapStyle={{ padding: 50 }}
                                            // labelHorizontal={true}
                                            onPress={(value) => { this.setState({ paymentMode: value }) }}
                                        />
                                        {/* <Image source={require('../../img/visa.png')} style={{ height: 15, width: 40 }} /> */}
                                        {/* <Text style={{ fontWeight: '500', marginLeft: 10 }}>$0.00 credits</Text> */}
                                    </View>
                                    <TouchableOpacity style={[Styles.AJ, { backgroundColor: '#636FA4', borderRadius: 3, height: 50, marginTop: 5 }]}
                                        // disabled={true}
                                        onPress={() => this.reserveSpot()}>
                                        <Text style={{ fontWeight: "500", color: '#fff' }}>
                                            Reserve Spot ₦{this.state.Amount}
                                        </Text>
                                        <Text style={{ color: '#fff', fontSize: 10 }}>Join the queue</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </HandleBack>
        )
    }
}
