import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity,Button,ActivityIndicator,Alert,RefreshControl } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
export default class UpcomingRide extends Component {
    constructor(props){
        super(props);
        this.state= {
            dateTime:null,
            route:null,
            RideDetail:null,
            isLoading: false,
            seats:null,
            refreshing: false,

        }
    }
    componentDidMount(){
        this._upcomingRides();
       
    }
    _upcomingRides(){
        url = global.ConstUrl + 'api/upcoming-rides/'+ global.Id
        fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': global.BarCode
            },
        })
            .then(response => response.json())
            .catch(error => {
              
                Alert.alert('Please Check Your Internet connection')
            })
            .then(response => {
                if (response.success == true) { 
                    this.setState({
                         RideDetail: response.data,
                         dateTime:response.data.start_time,
                         route:response.data.route_title ,
                         seats:response.data.available_seats 
                        })
                    console.warn(this.state.RideDetail)
                 } 
                 else if(response.success==false) {
                        alert(response.message)
                        this.props.navigation.navigate('CreateRide');

                }
            })
    }
    
    _onRefresh = () => {
        this.setState({ refreshing: false });
        this._upcomingRides();
    }
    render() {
        const RidesData = this.state.RideDetail
        if (!RidesData && !this.state.route && !this.state.dateTime) {
            return (
               <View style={{flex:1}}
               refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }>
                    <ActivityIndicator
                    animating={true}
                    style={Styles.indicator}
                    size='large'
                   
                /> 
               </View>  
            );
        }
        const pattren = '<.........>'
        return (

            <View style={{ flex: 1, margin: 20 }}>
                <ScrollView showsVerticalScrollIndicator={false}  refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }>
                {console.warn(this.state.route)}
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                {this.state.dateTime}
                       </Text>
                            <Text>
                                {this.state.route}
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>{this.state.seats} seats Available</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Wed. 23jan -16:00
                       </Text>
                            <Text>
                                Bethesda MD{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Avenue Elizabeth city
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>3 seats Available</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View><View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Thus. 24jan -18:00
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>3 seats Available</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View> */}
                    <View style={{marginTop:30}}>
                        <Button title="SCHEDULE A RIDE " color="#636FA4"/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
