import React, { Component } from 'react'
import { Alert, Text, View, Picker, SafeAreaView, TouchableOpacity, ScrollView, TextInput, ActivityIndicator, DatePickerAndroid, TimePickerAndroid, Modal } from 'react-native'
import Styles from '../../Stylesheet';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import DatePicker from 'react-native-datepicker-latest'
var hours = new Date().getHours(); //Current Hours
var min = new Date().getMinutes(); //Current Minutes
var date = new Date().getDate(); //Current Date
var month = new Date().getMonth() + 1; //Current Month
var year = new Date().getFullYear(); //Current Year
const routeId = null
const apiUrl = 'http://93.188.167.68:3100/api/'
export default class CreateRide extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dropLat: '',
            dropLong: '',
            pickUpLat: '',
            pickUpLong: '',
            vehicleId: '',
            userId: '',
            routeList: [],
            pickDropPoints: [],
            isLoading: false,
            curTime: null,
            curDate: null,
            animate: false,
            testt: null,
            Time_: null,
            Id_route: null,
            pick_up: {},
            drop_off: {},
            vehicleDetail: {}
        }
    }

    componentDidMount() {
        // this._BoardingPoint()
        let monthSet = month <= 9 ? "0" + month : month;
        let dateSet = date <= 9 ? "0" + date : date;
        let x = min <= 9 ? "0" + min : min;
        let y = hours <= 9 ? "0" + hours : hours;
        this.setState({ curTime: y + ":" + x, curDate: year + "-" + monthSet + "-" + dateSet });
        fetch(apiUrl + 'routes/list', {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response.json()).then(response => {
            console.log('RouteList', response)
            if (response.isSuccess) {
                response.data.splice(0, 0, {
                    description: "Select Route",
                    id: 1
                })
                this.getvehicle()
                this.getPickDropPoint()
                this.setState({
                    animate: false
                })
                this.setState({ routeList: response.data, })
                // console.log('id', response.data[0].id)

                // if (response.data[0].id) {
                //     this.getPickDropPoint(response.data[0].id, null)
                // }
            }
            else {
            }
        })
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
        this.getdriver()
    }
    getvehicle = () => {
        fetch(apiUrl + `vehicles/getVehicleDetail/${global.Id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response =>
            response.json()).then(response => {
                if (response.isSuccess) {
                    isLoading = false
                    this.setState({
                        vehicleDetail: response.data,
                    })
                    // availableVehicle = response
                    console.log('VehicleDetail', response.data)
                }
            })
            .catch(error => {
                console.log('Error:', error)
                this.setState({
                    animate: false
                })
            })
    }

    getdriver = () => {
        fetch(apiUrl + 'users/ ' + global.Id, {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response.json()).then(response => {
            console.log('user', response)
            if (response.isSuccess) {
                this.setState({
                    animate: false
                })
                // this.setState({ routeList: response.data, })
                // console.log('id', response.data[0].id)
                // if (response.data[0].id) {
                //     this.getPickDropPoint(response.data[0].id, null)
                // }
            }
            else {

            }
        })
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
    }

    getPickDropPoint = (id, key) => {
        if (!id) {
            return
        }
        console.log('pickDrop', id)

        this.setState({
            Id_route: id,
            animate: true
        })
        // url = global.ConstUrl + 'api/boardings/' + this.state.Id_route
        // console.warn(this.state.Id_route)

        fetch(apiUrl + 'pickDropPoint/' + id, {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response.json()).then(response => {
            console.log('pickDropPoint', response)
            this.setState({
                animate: false
            })

            if (response.isSuccess == true) {
                response.data.splice(0, 0, {
                    pickUpPoint: "Select Pickup Point",
                    dropPoint: "Select DropOff Point",
                    id: 1
                })
                console.log('pickDropPoints', response.data)
                this.setState({ pickDropPoints: response.data, animate: false })
            }
            else {
                Alert.alert('Somthing Went Wrong')
            }
        }).catch(error => {
            this.setState({
                animate: false
            })
            Alert.alert('Please Check Your Internet connection')
        })
    }

    async openAndroidDatePicker() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date()
            });
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    }
    async openAndroidTimePicker() {

        try {
            const { action, hour, minute } = await TimePickerAndroid.open({
                hour: 14,
                minute: 0,
                is24Hour: false,

            });
            if (action !== TimePickerAndroid.dismissedAction) {
                // Selected hour (0-23), minute (0-59)
                var x = minute <= 9 ? "0" + minute : minute;
                var y = hour <= 9 ? "0" + hour : hour;
                this.setState({ testt: y + ":" + x, });
            }

        } catch ({ code, message }) {

            console.warn('Cannot open time picker', message, '---------', code);
        }
    }

    _formatTime(hour, minute) {
        return hour + ':' + (minute < 10 ? '0' + minute : minute);
    }
    SucessMessage = () => {
        Alert.alert(
            "Congratulation",
            " Drive created sucessfully",
            [
                { text: "OK", onPress: () => this.props.navigation.navigate('MyRides') },
            ],
            { cancelable: false },
        );
        return true;

    }

    createDrive = () => {
        // alert('Ride Created Sucessfully')
        if (!this.state.vehicleDetail.id) {
            alert('Please Attach Your Vehicle First')
        } else {
            this.setState({
                animate: true
            })

            let data = {
                pickDropPointId: this.state.pick_up.id,
                vehicleId: this.state.vehicleDetail.id,
                userId: global.Id,
                departureTime: this.state.curTime,
            }

            console.log('create drive', data)
            var drive = JSON.stringify(data)
            fetch(apiUrl + 'drive/createDrive', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: drive

            }).then(response => response.json()).then(response => {
                this.setState({
                    animate: false
                })
                if (response.isSuccess) {
                    this.SucessMessage()
                }
                else {
                    console.log(response.error)
                }
            }).catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
        }

    }

    render() {

        const Pro = this.state.routeList
        if (!Pro) {
            return (
                <ActivityIndicator
                    animating={true}
                    style={Styles.indicator}
                    size='large'
                />
            );
        }

        return (

            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={Styles.HeaderRide}>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                            <FontAwesome name="bars" size={30} color="#fff" />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                            <Text style={{ color: '#fff', fontWeight: '500', }}>
                                Create Drive</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ margin: 30, backgroundColor: '#FFF', elevation: 10, padding: 20 }}>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Route</Text>

                                <Picker selectedValue={this.state.Id_route} onValueChange={this.getPickDropPoint} >

                                    {Pro.map((item, key) => {
                                        return <Picker.Item label={item.description} value={item.id} key={item.id} />
                                    }
                                        // <Text>{{ itemret }}</Text>

                                    )}

                                </Picker>
                            </View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Pickup Point</Text>
                                <Picker selectedValue={this.state.pick_up} onValueChange={(pick_up) => this.setState({ pick_up })}>
                                    {this.state.pickDropPoints.map((item, key) => {
                                        return <Picker.Item label={item.pickUpPoint} value={item} key={item.pickUpLong} />
                                    })
                                    }
                                </Picker>
                            </View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Dropoff Point</Text>
                                <Picker selectedValue={this.state.drop_off} onValueChange={(drop_off) => this.setState({ drop_off })}>
                                    {this.state.pickDropPoints.map((item, key) => {
                                        return <Picker.Item label={item.dropPoint} value={item} key={item.dropLong} />
                                    }
                                    )
                                    }
                                </Picker>
                            </View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Time</Text>
                                <Text style={{ color: "red", marginLeft: 10 }}>*you can select time only next 4 hours from current time </Text>
                                <View style={{ flexDirection: 'column' }}>

                                    <TouchableOpacity style={[]}>
                                        <DatePicker
                                            style={[Styles.DateTime, Styles.AJ]}
                                            date={this.state.curTime}
                                            mode="time"
                                            placeholder="select time"
                                            // format="h:mm:ss a"
                                            // minDate="2019-02-26"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 4,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 36
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ curTime: date }) }}
                                        />
                                        {/* <Text style={{ color: "#000", fontWeight: '500' }}>{this.state.curTime}</Text> */}
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <TouchableOpacity style={[Styles.AJ, { height: 50, backgroundColor: '#636FA4' }]} onPress={this.createDrive}>
                                <Text style={{ color: "#fff", fontWeight: '500' }}>CREATE DRIVE</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
                <Modal visible={this.state.animate} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.animate}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff"
                            />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </SafeAreaView>
        )
    }
}
