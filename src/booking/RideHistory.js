import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Button, RefreshControl, FlatList, ActivityIndicator, SafeAreaView } from 'react-native'
import Styles from '../../Stylesheet'

let isLoading = true;
export default class RideHistory extends Component {
    constructor() {
        super();
        this.getCurrentRide()
        this.state = {
            userData: []
        }
    }
    getCurrentRide = async () => {
        try {
            let response = await fetch(
                `http://93.188.167.68:3100/api/rides/getRides?id=${global.userId}&status=${'cencel'}`
            );
            let responseJson = await response.json();
            console.log('apiResponse', responseJson)
            if (responseJson.isSuccess) {
                isLoading = false
                this.setState({
                    userData: responseJson.data
                })
            }
        } catch (error) {
            console.error(error);
        }
    }

    renderScreen = (rides, noRide) => {
        return (
            <View style={{ flex: 1, margin: 20 }}>
                <View style={{ backgroundColor: '#636FA4', height: 50, marginTop: 20, marginBottom: 20, borderRadius: 3, justifyContent: 'center', paddingLeft: 20 }}>
                    <Text style={{ color: '#FFF', fontWeight: '500' }}>YOUR OFFERED RIDES</Text>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    {noRide == true ? <Text style={{ color: 'black' }}>No History Available</Text> : null}
                    {rides.map(ride => {
                        return (<View>
                            <View style={Styles.RideView}>
                                <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                                        Ride At
                  </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                                        {ride.rideAt}
                                    </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                                        Pick Up Loaction
                  </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                                        {ride.origin_addresses}
                                    </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                                        Destination
                  </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                                        {ride.destination_addresses}
                                    </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Seater</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{ride.seater}</Text>
                                </View>
                            </View>
                        </View>)
                    })}
                </ScrollView>
            </View>)
    }
    render() {
        if (isLoading) {
            return (
                <ActivityIndicator animating={true} style={Styles.indicator} size='large'
                />
            );
        } else {
            if (this.state.userData.length) {
                return this.renderScreen(this.state.userData, false)
            }
            else {
                return this.renderScreen([], true)
            }
        }

    }
}

