import React, { Component } from 'react'
import { Alert, Text, View, Image, SafeAreaView, ScrollView, TouchableOpacity, TextInput, ActivityIndicator, Modal, ImageBackground } from 'react-native'
import Styles from '../../Stylesheet';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import ImagePicker from 'react-native-image-picker'
const apiUrl = 'http://93.188.167.68:3100/api/vehicles/attactVehicle'
export default class CreateCar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            brandName: '',
            modelName: '',
            modelYear: '',
            vehicleNo: '',
            totalSeat: 4,
            vehicleLicenceNo: '',
            modelColor: '',
            // pickedImage: global.ConstUrl + 'images/' + global.Img,
            animate: false,
        };
    }
    SucessMessage = () => {
        Alert.alert(
            "Congratulation",
            " Car created sucessfully",
            [
                { text: "OK", onPress: () => this.props.navigation.navigate('CreateRide') },
            ],
            { cancelable: false },
        );
        return true;

    }


    attachCar = () => {
        this.setState({
            animate: true,
        })
        // url = global.ConstUrl + 'api/create-car'  //global.Id
        // console.warn('carData', this.state)
        let data = {
            brand: this.state.brandName,
            model: this.state.modelName,
            vehicleNo: this.state.vehicleNo,
            seater: this.state.seater,
            color: this.state.modelColor,
            year: this.state.modelYear,
            vehicleLicenceNo: this.state.vehicleLicenceNo,
            userId: global.Id
        }
        const vehicleDetail = JSON.stringify(data)
        console.log('attch', vehicleDetail)
        fetch(apiUrl, {
            method: 'post',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: vehicleDetail
        }).then(response => response.json()).then(response => {
            console.log('Attach car api Response', response)
            this.setState({
                animate: false
            })
            if (response.isSuccess) {
                this.SucessMessage()

            } else (
                alert(response.error)
            )
        }).catch(error => {
            console.log(error)
            this.setState({
                animate: false
            })
            Alert.alert('Please Check Your Internet connection')
        })
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: res.uri
                });
                global.Profile = this.state.pickedImage

            }
        });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView>
                    <View style={Styles.HeaderRide}>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                            <FontAwesome name="bars" size={30} color="#fff" />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                            <Text style={{ color: '#fff', fontWeight: '500', }}>
                                Attach Car</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ margin: 30, backgroundColor: '#FFF', elevation: 10, padding: 20 }}>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Enter Brand Name</Text> */}
                                <TextInput
                                    placeholder="Brand"
                                    style={{ borderBottomWidth: 1 }}
                                    onChangeText={(brandName) => this.setState({ brandName })}
                                />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Enter Model Name</Text> */}
                                <TextInput
                                    placeholder="Model"
                                    style={{ borderBottomWidth: 1 }}
                                    onChangeText={(modelName) => this.setState({ modelName })}
                                />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Enter Vehicle Number</Text> */}
                                <TextInput
                                    placeholder="Vehicle number"
                                    style={{ borderBottomWidth: 1 }}
                                    onChangeText={(vehicleNo) => this.setState({ vehicleNo })}
                                />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Enter Vehicle Color</Text> */}
                                <TextInput
                                    placeholder="Vehicle Color"
                                    style={{ borderBottomWidth: 1 }}
                                    onChangeText={(modelColor) => this.setState({ modelColor })}
                                />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Enter Model Year</Text> */}
                                <TextInput
                                    placeholder="Model Year"
                                    style={{ borderBottomWidth: 1 }} keyboardType='numeric'

                                    onChangeText={(modelYear) => this.setState({ modelYear })}
                                />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Seater</Text> */}
                                <TextInput
                                    placeholder="Seater" keyboardType='numeric'
                                    style={{ borderBottomWidth: 1 }} editable={false} selectTextOnFocus={false}
                                    onChangeText={(totalSeat) => this.setState({ totalSeat })} defaultValue={this.state.totalSeat = '4'}
                                />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                                {/* <Text style={{ fontSize: 15, fontWeight: "500", }}>Enter Car Licence No</Text> */}
                                <TextInput
                                    placeholder="Licence No"
                                    style={{ borderBottomWidth: 1 }}

                                    onChangeText={(vehicleLicenceNo) => this.setState({ vehicleLicenceNo })}
                                />
                            </View>
                            {/* <View style={{ marginBottom: 10 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", }}>Insurance</Text>
                                <TextInput
                                    placeholder="Insurance No" keyboardType='numeric'
                                    selectTextOnFocus={false}
                                    onChangeText={(insuranceNo) => this.setState({ insuranceNo })}
                                />
                                <TouchableOpacity onPress={this.pickImageHandler} style={{
                                    marginBottom: 10, alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <ImageBackground source={require('../assets/Upload_Document-512.png')} style={{ height: 150, width: 150, borderRadius: 100, }}>
                                        <Image

                                            style={{
                                                
                                                backgroundColor: '#d3d3d3'
                                            }}
                                            resizeMode='cover'
                                            source={{ uri: this.state.pickedImage }}
                                        />
                                    </ImageBackground>
                                </TouchableOpacity>
                            </View> */}
                            <TouchableOpacity style={[Styles.AJ, { height: 50, backgroundColor: '#636FA4' }]} onPress={this.attachCar}>
                                <Text style={{ color: "#fff", fontWeight: '500' }}>ATTACH CAR</Text>
                            </TouchableOpacity>

                        </View>

                    </View>
                    <Modal visible={this.state.animate} transparent={true} >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                        }}>
                            <View style={Styles.ModalView}>
                                <ActivityIndicator
                                    animating={this.state.animate}
                                    // style={Styles.indicator}
                                    style={{ justifyContent: 'center' }}
                                    size='large'
                                    color="#fff"
                                />
                                <View style={Styles.MV2}>
                                    <Text style={Styles.MVtext}>Please wait...</Text>
                                </View>
                            </View>
                        </View>

                    </Modal>
                </ScrollView>
            </SafeAreaView >
        )
    }
}
