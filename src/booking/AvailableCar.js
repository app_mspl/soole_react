import React, { Component } from 'react'
import { Text, AsyncStorage, View, SafeAreaView, ScrollView, ActivityIndicator, TextInput, Image, Dimensions, TouchableOpacity } from 'react-native'
import Styles from '../../Stylesheet';
import MapComponent from '../Gmap/GoogleMap';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { State } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window')

const apiUrl = 'http://93.188.167.68:3100/api/'
var userData = {}
var rideDetail = null
var placeId = null
var rideAt = null
// var isLoadi/ng = true
// var availableVehicle = []
export default class AvailableCar extends Component {

    constructor(props) {
        super(props);
        this.getvehicleList()
        this.state = {
            availableVehicle: [],
            selectVehicle: '#fff',
            vehicleId: null,
            isLoading: true,
            isDisabled: false
        }
        rideDetail = this.props.navigation.state.params.detail
        console.log('Avilabe', rideDetail)
        placeId = this.props.navigation.state.params.detail.locationDetail.pickDropLoction.pickUp.id
        console.log('placeId', placeId)
        rideAt = this.props.navigation.state.params.detail.rideAt
        console.log('rideAt', rideAt)
        AsyncStorage.getItem('userDetail').then(response => {
            response
            console.log('User:AvailableCar', response)
            userData = JSON.parse(response)
            console.log('pars:AvailableCar', userData)
        }).catch(error => {
            console.warn('Error:', error)
            this.setState({
                animate: false
            })
        });

    }

    getvehicleList = () => {
        console.log('abc', apiUrl + `drive/findDrive?id=${placeId}&rideAt=${rideAt}`)
        fetch(apiUrl + `drive/findDrive?id=${1}&rideAt=${"18:07"}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response.json()).then(response => {
            if (response.isSuccess) {
                // isLoading = false
                this.setState({
                    availableVehicle: response.items,
                    isLoading: false
                })
                console.log('VehicleList', response)
            } else {
                this.setState({

                    isLoading: false,
                    isDisabled: true
                })
                console.log('VehicleList', response)
                // isLoading = false
            }
        })
            .catch(error => {
                console.log('Error:', error)
                this.setState({
                    animate: false
                })
            })
    }
    // Signupp = async () => {
    bookRide = async (tripDetail) => {
        if (!this.state.vehicleId) {
            alert("Please Select Car")
        } else {
            console.log('bookRide', tripDetail)
            this.setState({
                animate: true
            })
            let data = {
                pickLatitude: tripDetail.locationDetail.pickDropLoction,
                pickLongitute: tripDetail.locationDetail.longitude,
                rideAt: tripDetail.rideAt,
                destination_addresses: tripDetail.locationDetail.destination,
                origin_addresses: tripDetail.locationDetail.currentAddresses,
                seater: tripDetail.seater,
                status: 'active',
                distance: tripDetail.locationDetail.distance,
                userId: userData.id,
                totalAmount: tripDetail.Amount,
                paymentMode: tripDetail.paymentMode,
                vehicleId: this.state.vehicleId
            }

            var bookride = JSON.stringify(data)
            console.log('stringifyData', bookride)
            fetch(apiUrl + 'rides/bookRide', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: bookride
            }).then(response =>
                response.json()).then(response => {
                    let data = response
                    console.log('apiResponse', data)
                    if (data.isSuccess) {
                        alert("Booking Successfully")
                    } else {
                        alert(response.error)
                    }
                })
                .catch(error => {
                    console.log('Error:', error)
                    this.setState({
                        animate: false
                    })
                })
        }

    }

    selectVehicle = (id) => {
        this.setState({
            vehicleId: id
        });
    }

    renderScreen = (rides, noRide) => {
        console.log('rides', rides)
        console.log('noRide', noRide)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView horizontal={false} >
                    <View style={Styles.HeaderRide}>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                            <FontAwesome name="bars" size={30} color="#fff" />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                            <Text style={{ color: '#fff', fontWeight: '500', }}>
                                Pick Car</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={{ padding: 30 }}>
                        <View style={Styles.ProfileDetail}>
                            <Text style={Styles.Name}>Pic Up Address</Text>
                            <Text>{rideDetail.locationDetail.pickDropLoction.pickUp.pickUpPoint}</Text>
                        </View>
                        <View style={Styles.ProfileDetail}>
                            <Text style={Styles.Name}>Drop Address</Text>
                            <Text>{rideDetail.locationDetail.pickDropLoction.pickUp.dropPoint}</Text>
                        </View>
                        <View style={Styles.ProfileDetail}>
                            <Text style={Styles.Name}>Seat</Text>
                            <Text>{rideDetail.seater}</Text>
                        </View>

                        <View style={Styles.ProfileDetail}>
                            <Text style={Styles.Name}>Ride At</Text>
                            <Text>{rideDetail.rideAt}</Text>
                        </View>
                        <View style={Styles.ProfileDetail}>
                            <Text style={Styles.Name}>Payment Method</Text>
                            <Text>{rideDetail.paymentMode}</Text>
                        </View>
                        <View style={Styles.ProfileDetail}>
                            <Text style={Styles.Name}>Total Amount</Text>
                            <Text>₦{rideDetail.Amount}</Text>
                        </View>

                    </View>
                    <View>
                        <ScrollView horizontal={true} >
                            <View style={{ height: 100, backgroundColor: '#fff', flexDirection: 'row', marginLeft: 10, marginRight: 10, }}>
                                {noRide == true ? <Text style={{ color: 'black', }}>No Vehicle Avilabe</Text> : null}
                                {rides.map(ride => {
                                    return (
                                        <TouchableOpacity style={{
                                            justifyContent: 'center',
                                            // paddingLeft: 10,
                                            backgroundColor: '#fff',

                                            width: 100,
                                            elevation: 5,
                                            borderRadius: 2, alignItems: 'center', marginRight: 10
                                        }}>
                                            <Text>{ride.departureTime}</Text>
                                            <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/30/000000/sedan.png' }} style={{ height: 40, width: 40 }} />
                                            <TouchableOpacity style={[Styles.AJ, { height: 30, backgroundColor: '#fff', width: "100%" }]} onPress={() => this.selectVehicle(ride.vehicleId)}>
                                                <Text style={{ fontWeight: '500', backgroundColor: this.state.vehicleId != ride.vehicleId ? 'white' : '#636FA4' }}>{ride.user.name}</Text>
                                            </TouchableOpacity >
                                        </TouchableOpacity>

                                    )
                                })
                                }

                            </View>
                        </ScrollView>
                    </View>
                    {/* <Image source={require('../../img/assets/cars.png')} style={{ width: width, height: 50, marginTop: 250 }} /> */}

                    <View style={{ margin: 40 }}>

                        <TouchableOpacity disabled={this.state.isDisabled} style={[Styles.AJ, { backgroundColor: '#636FA4', borderRadius: 3, height: 50, marginTop: 5 }]}
                            onPress={() => this.bookRide(rideDetail)}>
                            <Text style={{ fontWeight: "500", color: '#fff' }}>
                                Book Ride
                                    </Text>

                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView >
        )


    }
    render() {
        if (this.state.isLoading) {
            return (
                <ActivityIndicator
                    animating={true}
                    style={Styles.indicator}
                    size='large'
                />
            );
        } else {
            if (this.state.availableVehicle.length) {
                return this.renderScreen(this.state.availableVehicle, false)
            }
            else {
                return this.renderScreen([], true)
            }
        }

    }

}
