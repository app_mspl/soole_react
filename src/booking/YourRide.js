import React, { Component } from 'react'
import { Text, View,SafeAreaView,TouchableOpacity } from 'react-native'
import {createMaterialTopTabNavigator,createAppContainer} from 'react-navigation'
import CurrentRides from './CurrentRides'
import RideHistory from './RideHistory'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const RideTab=createMaterialTopTabNavigator({
    CurrentRides:{
    screen:CurrentRides,navigationOptions:{
        title:"CURRENT"
    }
},
RideHistory:{
    screen:RideHistory,navigationOptions:{
        title:"HISTORY"
    }
}
},
{
    tabBarOptions:{
        activeTintColor: '#636FA4',
        inactiveTintColor:'gray',
        indicatorStyle:{
            backgroundColor:'#636FA4',
            
            
        },
        
        style: {
            backgroundColor: '#fff',
            
          },
    }
}
)
const RideTab1=createAppContainer(RideTab)
export default class YourRide extends Component {
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
         <View style={{ paddingLeft: 20, height: 80, backgroundColor: '#636FA4', justifyContent: 'center' }}>
                    <Text style={{ color: '#fff', fontWeight: '500' }}>Your Rides</Text>
                </View>
                <RideTab1/>
                <View style={{height:50,backgroundColor:'#fff',elevation:10,flexDirection:'row',justifyContent:'center',alignItems:'center',paddingLeft:10,paddingRight:10}}>
                    <Text style={{textAlign:'center',flex:1.5,fontWeight:'500',color:'#636FA4',fontSize:20,}}>Rides</Text>
                    <TouchableOpacity style={{flex:1,backgroundColor:'blue'}}>
                        {/* <FontAwesome name='' size={} color=''/> */}
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>

                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>

                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>

                    </TouchableOpacity>
                </View>
      </SafeAreaView>
    )
  }
}
