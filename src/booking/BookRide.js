import React, { Component } from 'react'
import { Text, View, SafeAreaView, ScrollView, TextInput, Image, Dimensions, TouchableOpacity } from 'react-native'
import Styles from '../../Stylesheet';
import MapComponent from '../Gmap/GoogleMap';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const { width, height } = Dimensions.get('window')
export default class BookRide extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={Styles.HeaderRide}>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                        <FontAwesome name="bars" size={30} color="#fff" />
                    </TouchableOpacity>
                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                        <Text style={{ color: '#fff', fontWeight: '500', }}>
                            Book Your Ride</Text>
                    </View>
                    <View style={{ flex: 1 }} />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={[{ flex: 1, height: height }]}>
                        <MapComponent />
                        {/* <View style={{ marginTop: 35 }}>
                            <View style={Styles.MapoverView}>
                            <View style={{ height: 40, width: 10, backgroundColor: '#636FA4', marginRight: 10 }} />
                            <FontAwesome name='search' size={25} color='#b0b0b0' />
                            <TextInput
                                placeholder='Select Pickup Location'
                            />
                        </View>
                            <View style={Styles.MapoverView}>
                                <View style={{ height: 40, width: 10, backgroundColor: '#FF0000', marginRight: 10 }} />
                                <FontAwesome name='search' size={25} color='#b0b0b0' />
                                <TextInput
                                    placeholder='Select Drop Location'
                                />
                            </View>
                        </View> */}
                    </View>
                </ScrollView>
                {/* <View style={{height:100,backgroundColor:'#fff',flexDirection:'row',marginLeft:10,marginRight:10}}>
                <TouchableOpacity style={{flex:1,alignItems:'center',}}>
                    <Text>08:00 AM</Text>
                    <Image source={{uri:'https://img.icons8.com/ios-glyphs/30/000000/sedan.png'}} style={{height:40,width:40}}/>
                    <TouchableOpacity style={[Styles.AJ,{height:30,backgroundColor:'#636FA4',width:"100%"}]}>
                    <Text style={{color:'#fff',fontWeight:'500'}}>John</Text>
                    </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1,alignItems:'center'}}>
                    <Text>10:00 AM</Text>
                    <Image source={{uri:'https://img.icons8.com/ios-glyphs/30/000000/sedan.png'}} style={{height:40,width:40}}/>
                    <TouchableOpacity style={[Styles.AJ,{height:30,backgroundColor:'#fff',width:"100%"}]}>
                    <Text style={{fontWeight:'500'}}>Depp</Text>
                    </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1,alignItems:'center'}}>
                    <Text>12:00 PM</Text>
                    <Image source={{uri:'https://img.icons8.com/ios-glyphs/30/000000/sedan.png'}} style={{height:40,width:40}}/>
                    <TouchableOpacity style={[Styles.AJ,{height:30,backgroundColor:'#fff',width:"100%"}]}>
                    <Text style={{fontWeight:'500'}}>Alfando</Text>
                    </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1,alignItems:'center'}}>
                    <Text>No Cab</Text>
                    <Image source={{uri:'https://img.icons8.com/ios-glyphs/30/000000/sedan.png'}} style={{height:40,width:40,opacity:0.3}}/>
                    <TouchableOpacity style={[Styles.AJ,{height:30,backgroundColor:'#fff',width:"100%"}]}>
                    <Text style={{fontWeight:'500'}}>Michale</Text>
                    </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1,alignItems:'center'}}>
                    <Text>04:00 PM</Text>
                    <Image source={{uri:'https://img.icons8.com/ios-glyphs/30/000000/sedan.png'}} style={{height:40,width:40}}/>
                    <TouchableOpacity style={[Styles.AJ,{height:30,backgroundColor:'#fff',width:"100%"}]}>
                    <Text style={{fontWeight:'500'}}>sam</Text>
                    </TouchableOpacity>
                </TouchableOpacity>
                 <Image source={require('../../img/assets/cars.png')} style={{width:width,height:50}}/> 
                </View> */}
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={[Styles.AJ, { flex: 1, height: 40, backgroundColor: "#6d6d6d", marginRight: 5 }]} onPress={() => this.props.navigation.navigate('FindRide')}>
                        <Text style={{ color: '#fff', fontWeight: '500' }}>RIDE LATER</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[Styles.AJ, { flex: 1, height: 40, backgroundColor: "#000", marginLeft: 5 }]} onPress={() => this.props.navigation.navigate('AvailableCar')}>
                        <Text style={{ color: '#fff', fontWeight: '500' }}>RIDE NOW</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}
