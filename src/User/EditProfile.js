import React, { Component } from 'react'
import { Text, View, SafeAreaView, ScrollView, TextInput, Image, Switch, TouchableOpacity, ImageBackground } from 'react-native'
import Styles from '../../Stylesheet'
import ImagePicker from 'react-native-image-picker'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const apiUrl = 'http://93.188.167.68:3100/api/vehicles/updateVehicleDetial/'
export default class EditProfile extends Component {
  constructor() {
    super();
    this.getCarDetail()
    this.state = {

      brandName: '',
      modelName: '',
      modelYear: '',
      vehicleNo: '',
      totalSeat: 4,
      vehicleLicenceNo: '',
      modelColor: '',
      switch1Value: false,
      pickedImage: global.ConstUrl + 'images/' + global.Img,
      animate: false,

    };
  }
  getCarDetail = async () => {

    try {
      let response = await fetch(

        `http://93.188.167.68:3100/api/vehicles/getVehicleDetail/${global.Id}`

      );
      let responseJson = await response.json();
      console.log(' get vehicle api Response :', responseJson)
      if (responseJson.isSuccess) {
        isLoading = false
        var data = {
          brandName: responseJson.data.brand,
          modelName: responseJson.data.model,
          modelYear: responseJson.data.year,
          vehicleNo: responseJson.data.vehicleNo,
          vehicleLicenceNo: responseJson.data.vehicleLicenceNo,
          modelColor: responseJson.data.color,
        }
        this.setState({
          brandName: responseJson.data.brand,
          modelName: responseJson.data.model,
          modelYear: responseJson.data.year,
          vehicleNo: responseJson.data.vehicleNo,
          totalSeat: 4,
          vehicleLicenceNo: responseJson.data.vehicleLicenceNo,
          modelColor: responseJson.data.color,
          switch1Value: false,
          pickedImage: global.ConstUrl + 'images/' + global.Img,
          animate: false,
        })
        console.log('detail', this.state)
      }
    } catch (error) {
      console.error(error);
    }
  }
  SucessMessage = () => {
    Alert.alert(
      "Congratulation",
      " Car created sucessfully",
      [
        { text: "OK", onPress: () => this.props.navigation.navigate('CreateRide') },
      ],
      { cancelable: false },
    );
    return true;

  }
  attachCar = () => {
    // this.setState({
    //   animate: true,
    // })
    // url = global.ConstUrl + 'api/create-car'  //global.Id
    // console.warn('carData', this.state)
    let data = {
      brand: this.state.brandName,
      model: this.state.modelName,
      vehicleNo: this.state.vehicleNo,
      seater: this.state.seater,
      color: this.state.modelColor,
      year: this.state.modelYear,
      vehicleLicenceNo: this.state.vehicleLicenceNo,
    }
    const vehicleDetail = JSON.stringify(data)
    console.log('edit attch vehicle', vehicleDetail)
    fetch(apiUrl + global.Id, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: vehicleDetail
    }).then(response => response.json()).then(response => {
      console.log('Attach car update api Response', response)
      // this.setState({
      //   animate: false
      // })
      if (response.isSuccess) {
        this.getCarDetail()
        this.SucessMessage()
      }
    }).catch(error => {
      // this.setState({
      //   animate: false
      // })
      Alert.alert('Please Check Your Internet connection')
    })
  }

  toggleSwitch1 = (value) => {
    // this.setState({ switch1Value: value }) //condition ? if : else
    // value == true ? this.setState({ RegNo: 'DL 1A 0000' }) : this.setState({ RegNo: '' })
    // console.warn('Switch 1 is: ' + value)
  }
  pickImageHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        // this.setState({
        //   pickedImage: res.uri
        // });
        // global.Profile = this.state.pickedImage
      }
    });
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={Styles.HeaderRide}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
            <FontAwesome name="bars" size={30} color="#fff" />
          </TouchableOpacity>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
            <Text style={{ color: '#fff', fontWeight: '500', }}>
              Driver Setting</Text>
          </View>
          <View style={{ flex: 1 }} />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ flexDirection: 'row', margin: 10, }}>
            <View style={{ flex: 1 }} />
            <View style={[Styles.AJ, { flex: 3, }]}>
              <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 150, width: 150, borderRadius: 100, }}>
                <Image source={{ uri: global.Profile }} style={{ height: 150, width: 150, borderRadius: 100 }} />
              </ImageBackground>
              <Text style={Styles.Name}>{global.name}</Text>
            </View>
            <View style={{ flex: 1 }}>

            </View>
          </View>
          <View style={{ height: 90, flexDirection: 'row', elevation: 10, backgroundColor: '#636FA4' }}>
            <View style={[Styles.AJ, { flex: 1 }]}>
              <Text style={Styles.RatingTxt}>4.77</Text>
              <Text style={{ color: '#fff' }}>Rating</Text>
            </View>
            <TouchableOpacity style={[Styles.AJ, { flex: 1 }]} onPress={() => this.props.navigation.navigate('MyRides')}>
              <Text style={Styles.RatingTxt}>2</Text>
              <Text style={{ color: '#fff' }}>Rides</Text>
            </TouchableOpacity>

          </View>
          {/* <View style={[Styles.AJ, { margin: 20 }]}>
            <View style={{ flexDirection: 'row', borderWidth: 2, borderColor: '#636FA4', height: 40, minWidth: 175, borderRadius: 5, alignItems: 'center', paddingLeft: 5 }}>
              <Text>REG NO:  </Text>
              <Text>{this.state.RegNo}</Text>
            </View>
          </View> */}
          <View style={{ flex: 1 }}>
            <View style={{ margin: 30, backgroundColor: '#FFF', elevation: 10, padding: 20 }}>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Brand Name</Text>
                <TextInput
                  placeholder="Brand"
                  style={{ borderBottomWidth: 1 }}
                  onChangeText={(brandName) => this.setState({
                    brandName
                  })} value={this.state.brandName}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Model Name</Text>
                <TextInput
                  placeholder="Model"
                  style={{ borderBottomWidth: 1 }}
                  onChangeText={(modelName) => this.setState({
                    modelName
                  })} value={this.state.modelName}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Vehicle Number</Text>
                <TextInput
                  placeholder="Vehicle number"
                  style={{ borderBottomWidth: 1 }}
                  onChangeText={(vehicleNo) => this.setState({
                    vehicleNo
                  })} value={this.state.vehicleNo}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Vehicle Color</Text>
                <TextInput
                  placeholder="Vehicle Color"
                  style={{ borderBottomWidth: 1 }}
                  onChangeText={(modelcolor) => this.setState({
                    modelcolor
                  })} value={this.state.modelColor}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Model Year</Text>
                <TextInput
                  placeholder="Model Year"
                  style={{ borderBottomWidth: 1 }} keyboardType='numeric'
                  onChangeText={(modelYear) => this.setState({
                    modelYear
                  })} value={this.state.modelYear}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Seater</Text>
                <TextInput
                  placeholder="Seater" keyboardType='numeric'
                  style={{ borderBottomWidth: 1 }} editable={false} selectTextOnFocus={false}
                  onChangeText={(totalSeat) => this.setState({
                    totalSeat: 4
                  })} value={this.state.totalSeat = '4'}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "500", }}>Car Licence No</Text>
                <TextInput
                  placeholder="Licence No"
                  style={{ borderBottomWidth: 1 }}
                  onChangeText={(vehicleLicenceNo) => this.setState({
                    vehicleLicenceNo
                  })} value={this.state.vehicleLicenceNo}
                />
              </View>
              <TouchableOpacity style={[Styles.AJ, { height: 50, backgroundColor: '#636FA4' }]} onPress={this.attachCar}>
                <Text style={{ color: "#fff", fontWeight: '500' }}>UPDATE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView >
      </SafeAreaView >
    )
  }
}
