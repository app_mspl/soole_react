import React, { Component } from 'react'
import { Text, View,SafeAreaView } from 'react-native'
import Logo from '../Logo';
import LinearGradient from 'react-native-linear-gradient';
import Styles from '../../Stylesheet';

export default class AboutUs extends Component {
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
      <LinearGradient colors={global.ColorG} style={{ flex: 1 }}>
                 
            <View style={[{flex:1,},Styles.AJ]}>
                <Logo/>
            </View>
            <View style={[{flex:2,alignItems:'center'}]}>
                      <Text style={{ color: '#fff', fontSize: 25, fontWeight: 'bold' }}>
                          About Us
                      </Text>
                      <View style={{margin:20}}> 
                      <Text style={{color:'#fff',textAlign:'center',fontSize:15}}>
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galleyLorem Ipsum is simply dummy text of the printing.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galleyLorem Ipsum is simply dummy text of the printing.
                      </Text>
                      </View>
                  </View>
            </LinearGradient>
      </SafeAreaView>
    )
  }
}
