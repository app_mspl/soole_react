import React, { Component } from 'react'
import { Alert, Text, View, SafeAreaView, Image, TextInput, Button, ScrollView, CameraRoll, TouchableOpacity, Modal, ActivityIndicator, ImageBackground } from 'react-native'
import Styles from '../../Stylesheet'
//import PhotoUpload from 'react-native-photo-upload'
// import ImagePicker from 'react-native-customized-image-picker';
import ImagePicker from 'react-native-image-picker'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const apiUrl = 'http://93.188.167.68:3100'
export default class DriverEditProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      mobileNo: '',
      email: '',
      profilePic: {},
      pic: "",
      licencePic: '',
      animate: false,
      uploadStatus: ''
    }
  }
  componentDidMount() {
    this.getUser()
  }
  SucessMessage = () => {
    Alert.alert(
      "",
      " Profile Update sucessfully",
      [
        { text: "OK", onPress: () => this.props.navigation.navigate('FindRide') },
      ],
      { cancelable: false },
    );
    return true;

  }
  getUser = () => {
    fetch(apiUrl + `/api/users/${global.Id}`, {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },

    }).then(response => response.json()).then(response => {
      console.log('user Data:', response)
      if (response.isSuccess) {
        this.setState({
          animate: false,
          name: response.data.name,
          mobileNo: response.data.mobile.toString(),
          pic: response.data.profileUrl,
          email: response.data.email
        })
        console.log('detail', this.state)
      } else {
        alert(response.error)
      }
    })
      .catch(error => {
        console.warn('Error:', error)
        this.setState({
          animate: false
        })
      })
  }
  _Submit = () => {
    console.warn(this.state)
    this.setState({
      animate: true
    })

    var formdata = new FormData();
    formdata.append('name', this.state.name);
    formdata.append('mobile', this.state.mobileNo);
    formdata.append("file", {
      uri: this.state.profilePic.uri,
      name: this.state.profilePic.fileName,
      type: "multipart/form-data"
    });
    fetch(apiUrl + `/api/users/update/${global.Id}`, {
      method: 'put',
      headers: {
        'Content-Type': 'multipart/form-data',
        // 'Authorization': global.BarCode
      },
      body: formdata
    })
      .then(response => response.json()).then(response => {
        console.warn('user Data:', response)
        if (response.isSuccess) {
          this.setState({
            animate: false,
            name: response.data.name,
            mobile: response.data.mobile.toString(),
            pic: response.data.profileUrl,
            email: response.data.email

          })
          this.getUser()
          this.SucessMessage()
          console.log('detail', this.state)
        } else {
          alert(response.error)
        }
      })
      .catch(error => {
        console.warn('Error:', error)
        this.setState({
          animate: false
        })
      })


  }
  profilePickerHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        this.setState({
          profilePic: res
        });

      }
    });
  }

  licencePickerHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        this.setState({
          licencePic: res.uri,
          uploadStatus: 'success'
        });
        // global.licence = this.state.licencePic

      }
    });
  }
  render() {
    // global.ProfileImg=this.state.profilePic
    return (
      <SafeAreaView style={{ flex: 1 }}>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={Styles.HeaderRide}>
            <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
              <FontAwesome name="bars" size={30} color="#fff" />
            </TouchableOpacity>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
              <Text style={{ color: '#fff', fontWeight: '500', }}>
                Edit Profile</Text>
            </View>
            <View style={{ flex: 1 }} />
          </View>
          <View style={{ flex: 1, marginTop: 20 }}>
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
              <View style={[Styles.AJ]}>
                <TouchableOpacity onPress={this.profilePickerHandler}>
                  <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 150, width: 150, borderRadius: 100, }}>
                    <Image
                      style={{
                        paddingVertical: 30,
                        width: 150,
                        height: 150,
                        borderRadius: 75,
                        backgroundColor: '#d3d3d3'
                      }}
                      resizeMode='cover'
                      source={{ uri: this.state.pic }}
                    />
                  </ImageBackground>
                </TouchableOpacity>
                {/* <Image source={require('../../img/profile_user.jpg')} style={{ height: 150, width: 150, borderRadius: 100 }} /> */}
                <Text style={Styles.Name}>{this.state.name}</Text>
              </View>
            </View>
            <View style={{ flex: 1.5, margin: 20 }}>
              <TextInput
                placeholder='Name'
                onChangeText={(name) => this.setState({ name })} value={this.state.name}
                returnKeyType='next'

                style={[Styles.LoginText, { marginTop: 10 }]}
              />

              <TextInput
                placeholder='Mobile No'
                keyboardType='numeric'
                onChangeText={(mobileNo) => this.setState({ mobileNo })} value={this.state.mobileNo}
                returnKeyType='done'
                style={[Styles.LoginText, { marginTop: 20 }]}
              />
              <TextInput
                placeholder='Email'
                editable={false}
                keyboardType='email-address'
                value={this.state.email}
                returnKeyType='done'
                style={[Styles.LoginText, { marginTop: 20 }]}
              />
              <View style={[Styles.ProfileDetail, { flexDirection: 'row', justifyContent: 'center' }]}>
                <View>
                  <Text style={{ marginTop: 10 }}>Upload your Licence</Text>
                  <Text>{this.state.uploadStatus}</Text>
                </View>

                <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 10, justifyContent: 'center' }}>
                  <TouchableOpacity onPress={this.licencePickerHandler}>
                    <FontAwesome name="upload" size={25} color="#000" />
                  </TouchableOpacity>
                </View>

              </View>
              <View style={{ marginTop: 30 }}>
                <Button title="SUBMIT" color="#636FA4" onPress={this._Submit} />
              </View>
            </View>
          </View>
        </ScrollView>
        <Modal visible={this.state.animate} transparent={true} >
          <View style={{
            flex: 1,
            justifyContent: 'center',
          }}>
            <View style={Styles.ModalView}>
              <ActivityIndicator
                animating={this.state.animate}
                // style={Styles.indicator}
                style={{ justifyContent: 'center' }}
                size='large'
                color="#fff"
              />
              <View style={Styles.MV2}>
                <Text style={Styles.MVtext}>Please wait...</Text>
              </View>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    )
  }
}
