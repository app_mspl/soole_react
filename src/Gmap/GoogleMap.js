import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  Picker, ScrollView, ActivityIndicator, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import MapView, { Marker, Callout, PolyLine } from 'react-native-maps';
import Styles from '../../Stylesheet'
const { width, height } = Dimensions.get('window')

import Polyline from '@mapbox/polyline'

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.5
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
const apiUrl = 'http://93.188.167.68:3100/api/'
class MapComponent extends Component {

  constructor() {

    super()

    this.state = {
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
        predications: [],
        pointcoords: [],
        // dropLoaction: {},
        distance: '',
        duration: '',
        destination: '',
        currentAddresses: '',
        pickDropLoction: {
          pickUp: {},
          dropOff: {}
        }
      }, routeList: [],
      pickDropPoints: [],
      showRoute: true,
      showPlaces: false,
      pickUpLat: '',
      Id_route: null,
      picUpLong: '',
      dropOffLat: '',
      dropOffLong: '',
      pickUpLocation: {},
      dropOffLocation: {},
      animate: false
    }

  }

  componentDidMount() {
    fetch(apiUrl + 'routes/list', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json()).then(response => {
        console.log('RouteList', response)
        if (response.isSuccess) {
          response.data
          response.data.splice(0, 0, {
            description: "Select Route",
            id: 1
          })
          this.setState({
            animate: false
          })
          this.setState({ routeList: response.data })
          console.log('id', response.data[0].id)
        }
        else {

        }
      })
      .catch(error => {
        this.setState({
          animate: false

        })
        Alert.alert('Please Check Your Internet connection')
      })
    navigator.geolocation.getCurrentPosition((position) => {

      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)

      console.log('getCurrentPosition')
      console.log('latLoction', lat)
      // this.getPickDropPoint(lat, long)
      console.log('position', position)
      console.log('longLoction', long)
      var initialRegion = {
        latitude: lat,
        longitude: long,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
        predications: [],
        pointcoords: []

      }
      this.setState({ initialPosition: initialRegion })

    },
      (error) => alert(JSON.stringify(error)),
      { enableHighAccuracy: false, timeout: 20000, });
  }

  getPickDropPoint = (id, key) => {

    if (!id) {
      return
    }
    console.log('pickDrop', id)

    this.setState({
      Id_route: id,
      animate: true,
      showPlaces: true,
      showRoute: false
    })


    fetch(apiUrl + 'pickDropPoint/' + id, {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(response => response.json()).then(response => {
      console.log('pickDropPoint', response)
      this.setState({
        animate: false
      })

      if (response.isSuccess == true) {
        response.data.splice(0, 0, {
          pickUpPoint: "Select Pickup Point",
          dropPoint: "Select DropOff Point",
          id: 1
        })
        console.log('pickDropPoints', response.data)
        this.setState({ pickDropPoints: response.data, animate: false })
      }
      else {
        Alert.alert('Somthing Went Wrong')
      }
    }).catch(error => {
      this.setState({
        animate: false
      })
      Alert.alert('Please Check Your Internet connection')
    })
    // console.log("called")
    // // console.log('pickDrop', key)

    // console.log(apiUrl + `pickDropPoint/?lat=${lat}&long=${long}`)
    // fetch(apiUrl + `pickDropPoint/?lat=${lat}&long=${long}`, {
    //   method: 'get',
    //   headers: {
    //     Accept: 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    // }).then(response => response.json()).then(response => {
    //   this.setState({
    //     animate: false
    //   })
    //   console.log('jjjjj', response)
    //   if (response.isSuccess) {
    //     console.log('pickDropPoints', response.data)
    //     response.data.splice(0, 0, {
    //       pickUpPoint: "Select Pickup Point",
    //       dropPoint: "Select DropOff Point",
    //       id: 1
    //     })
    //     this.setState({ pickDropPoints: response.data, animate: false })
    //   }
    // }).catch(error => {
    //   this.setState({
    //     animate: false
    //   })
    //   Alert.alert('Please Check Your Internet connection')
    // })
  }

  async getDirections(route) {

    console.log(`route ${route}`)
    if (!route.pickUpPoint) {
      alert('Please Select Pickup DropOff Point')
    } else {
      this.setState({
        showRoute: false,
        showPlaces: false
      })
      console.log(`pic ${this.state.pickUpLocation}`)
      console.log(`drop ${this.state.dropOffLocation}`)
      // this.pickUpLocation
      // this.dropOffLocation
      try {
        const data = await fetch(
          `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${this.state.pickUpLocation.pickUpLat},${this.state.pickUpLocation.pickUpLong} &destinations=${this.state.pickUpLocation.dropLat},${this.state.pickUpLocation.dropLong}&mode:driving&key=AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU`
        )
        const distanceDetail = await data.json();
        console.log('distanceresponse Response', distanceDetail)
        let distance = distanceDetail.rows[0].elements[0].distance.text
        let totalTime = distanceDetail.rows[0].elements[0].duration.text
        let destinationAddresses = distanceDetail.destination_addresses[0]
        let originAddresses = distanceDetail.origin_addresses[0]
        const response = await fetch(
          `https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.pickUpLocation.pickUpLat},${this.state.pickUpLocation.pickUpLong}&destination=${this.state.pickUpLocation.dropLat},${this.state.pickUpLocation.dropLong}&key=AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU`
        )
        const json = await response.json();
        console.log('GetDriecton Response', json)
        const points = Polyline.decode(json.routes[0].overview_polyline.points);
        console.log('points', points)
        const pointcoords = points.map(point => {
          return {
            latitude: point[0],
            longitude: point[1]
          }
        })
        var setDirection = {
          latitude: parseFloat(this.state.pickUpLocation.pickUpLat),
          longitude: parseFloat(this.state.pickUpLocation.pickUpLong),
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
          predications: [],
          pointcoords: pointcoords,
          pickDropLoction: {
            pickUp: this.state.pickUpLocation,
            dropOff: this.state.dropOffLocation
          },
          distance: distance,
          duration: totalTime,
          destination: destinationAddresses,
          currentAddresses: originAddresses
        }
        console.log('new', setDirection)
        this.setState({ initialPosition: setDirection })
        this.props.onSelectLoction(this.state.initialPosition);
      } catch{
        // (error)
        // console.log(error)
      }
    }
  }


  renderScreen = () => {
    const routeList = this.state.routeList

    let marker = null;

    if (this.state.initialPosition.pointcoords.length > 1) {
      marker = (
        <Marker
          coordinate={this.state.initialPosition.pointcoords[this.state.initialPosition.pointcoords.length - 1]}
        >
          <Image
            source={{ uri: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png' }}
            style={{ width: 30, height: 30 }}
          />
          <View>
            <Text style={{ fontSize: 15, color: "black" }}>
              {this.state.initialPosition.duration}
            </Text>
          </View>
        </Marker>
        // <Marker.Animated
        //   coordinate={this.state.initialPosition.pointcoords[this.state.initialPosition.pointcoords.length - 1]}
        // />
      )
    }
    return (
      <View style={styles.container}>
        <MapView
          fitToElements={true}
          // minZoomLevel={2}  // default => 0
          // maxZoomLevel={7}
          style={styles.map}
          showsUserLocation={true}
          region={this.state.initialPosition}

        // onRegionChange={this.onRegionChange}
        >
          <MapView.Polyline
            coordinates={this.state.initialPosition.pointcoords}
            strokeWidth={6}
            strokeColor="blue"
          />
          {marker}
          <Marker
            coordinate={this.state.initialPosition}
          >
            <Image
              source={{ uri: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png' }}
              style={{ width: 30, height: 30 }}
            />
            <View>
              <Text>
                marker text
          </Text>
            </View>
          </Marker>
        </MapView>
        <View>
        </View>
        <View style={{ marginTop: 0, width: 300 }}>
          <View style={{ margin: 10, backgroundColor: '#FFF', elevation: 10, padding: 10 }}>
            <View>
              {/* <View style={{ marginBottom: 20 }}> */}
              {this.state.showPlaces === true ?
                <View>
                  <TouchableOpacity onPress={() => this.setState({
                    showPlaces: false,
                    showRoute: true
                  })}>
                    <Text style={{ color: "black" }}>Back</Text>
                  </TouchableOpacity>
                  <View style={{ marginBottom: 20 }}>
                    {/* <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Pickup Point</Text> */}
                    {/* <ActivityIndicator
                      animating={this.state.animate}
                      
                      style={{ justifyContent: 'center' }}
                      size='large'
                      color="#fff"
                    /> */}
                    <Picker selectedValue={this.state.pickUpLocation} onValueChange={(pick_up) => {
                      this.setState({
                        pickUpLocation: pick_up
                      })
                    }}>
                      {this.state.pickDropPoints.map((item, key) => {
                        return <Picker.Item label={item.pickUpPoint} value={item} key={key} />
                      })}
                    </Picker>
                  </View>
                  <View style={{ marginBottom: 20 }}>
                    {/* <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Dropoff Point</Text> */}
                    <Picker selectedValue={this.state.dropOffLocation} onValueChange={(dropOff) => this.setState({
                      dropOffLocation: dropOff
                    })}>
                      {this.state.pickDropPoints.map((item, key) => {
                        return <Picker.Item label={item.dropPoint} value={item} key={key} />
                      })}
                    </Picker>
                    <TouchableOpacity style={{ padding: 10, backgroundColor: "black" }} onPress={() => this.getDirections(this.state.pickUpLocation)}>
                      <Text style={{ fontSize: 15, fontWeight: "500", color: "white", textAlign: "center" }}>Done</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                :
                <Picker selectedValue={this.state.Id_route} onValueChange={this.getPickDropPoint} >
                  {routeList.map((item, key) => {
                    return <Picker.Item label={item.description} value={item.id} key={key} />
                  })}

                </Picker>}
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {

    const Pro = this.state.initialPosition
    if (!Pro) {
      return (
        <ActivityIndicator
          animating={true}
          style={Styles.indicator}
          size='large'
        />
      );
    }
    return (
      this.renderScreen()
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
const pickerSelectStyles = StyleSheet.create({

  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'gray',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

export default MapComponent;