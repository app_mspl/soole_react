import React, { Component } from 'react'
import { Alert, Text, View, TextInput, TouchableOpacity, Modal, SafeAreaView, ScrollView, ActivityIndicator, WebView } from 'react-native'
import Styles from '../../Stylesheet'
import LinearGradient from 'react-native-linear-gradient'
import CheckBox from 'react-native-check-box'
export default class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      password: '',
      Confirmpass: '',
      animate: false,
      modalPolicy: false,
      isChecked: false,

    }
  }
  _privacyPolicy = () => {
    if (!this.state.password || !this.state.email || !this.state.Confirmpass || !this.state.username) {
      this.signUp()
    } else {
      this.setState({ modalPolicy: true })
    }
  }
  _privacyPolicy2 = () => {
    this.setState({ modalPolicy: false })
  }
  _privacyPolicy1 = () => {
    if (this.state.isChecked) {
      this.setState({ modalPolicy: false })
      this.signUp()
    }
  }
  SucessMessage = () => {
    Alert.alert(
      "Congratulation",
      " Account created sucessfully",
      [
        { text: "OK", onPress: () => this.props.navigation.navigate('Login') },
      ],
      { cancelable: false },
    );
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <LinearGradient colors={global.ColorG} style={{ flex: 1 }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ height: 100 }} />
            <View style={Styles.ForgetPassLog}>
              <Text style={{ color: '#fff', fontSize: 25, fontWeight: 'bold' }}>SIGN UP IN SECONDS</Text>
              <Text style={{ color: '#fff', marginTop: 10 }}>Signup using your Email address</Text>
            </View>
            <View style={{ flex: 3, }}>
              <View style={{ margin: 20 }}>
                <TextInput
                  returnKeyType='next'
                  placeholder='Your Name'
                  onChangeText={(username) => this.setState({ username })}
                  style={[Styles.LoginText, { marginTop: 10 }]}
                />
                <TextInput
                  placeholder='Email Id'
                  keyboardType='email-address'
                  returnKeyType='next'
                  onChangeText={(email) => this.setState({ email })}
                  style={[Styles.LoginText, { marginTop: 10 }]}

                />
                <TextInput
                  placeholder='Password'
                  returnKeyType='next'
                  onChangeText={(password) => this.setState({ password })}
                  secureTextEntry
                  style={[Styles.LoginText, { marginTop: 10 }]}

                />
                <TextInput
                  placeholder='Confirm Password'
                  onChangeText={(Confirmpass) => this.setState({ Confirmpass })}
                  secureTextEntry
                  style={[Styles.LoginText, { marginTop: 10 }]}
                />
              </View>
              <TouchableOpacity style={[Styles.LoginBtn, Styles.AJ]} onPress={this._privacyPolicy}>
                <Text style={Styles.LoginBtnTxt}>CREATE MY ACCOUNT</Text>

              </TouchableOpacity>
              <View style={{ flex: 1, flexDirection: 'row', margin: 20 }}>
                {/* <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                <Text style={{ color: '#fff', textDecorationLine: 'underline' }}>
                                    Forget Password</Text>
                            </TouchableOpacity> */}
                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: "row" }}>
                  <Text style={{ color: '#fff' }}>
                    Already have an Account?</Text>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={{ color: '#fff' }}> Login here</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ScrollView>
          <Modal visible={this.state.modalPolicy} transparent={true}>
            <View style={{ flex: 1, margin: 20, backgroundColor: "#fff", borderRadius: 5 }}>
              <Text style={{ fontSize: 25, color: '#000', marginLeft: 20, marginTop: 10 }}>Privacy Polocy</Text>
              <View style={{ flex: 6, margin: 20, borderColor: "#000", borderWidth: 1 }}>
                <WebView
                  source={{ uri: 'https://meandersoftware.co/policy' }}
                  style={{ marginTop: 5 }}
                />
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <CheckBox
                  style={{ padding: 10 }}
                  onClick={() => {
                    this.setState({
                      isChecked: !this.state.isChecked
                    })
                  }}
                  isChecked={this.state.isChecked}
                  leftText={""}
                />
                <TouchableOpacity style={[{ backgroundColor: '#636fa4', height: 40, width: 80, marginLeft: 20, borderRadius: 5 }, Styles.AJ]} onPress={this._privacyPolicy1}>
                  <Text style={{ color: '#fff' }}>Agree</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[{ backgroundColor: '#b0b0b0', height: 40, width: 80, marginLeft: 20, borderRadius: 5 }, Styles.AJ]} onPress={this._privacyPolicy2}>
                  <Text>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Modal visible={this.state.animate} transparent={true} >
            <View style={{
              flex: 1,
              justifyContent: 'center',
            }}>
              <View style={Styles.ModalView}>
                <ActivityIndicator
                  animating={this.state.animate}
                  // style={Styles.indicator}
                  style={{ justifyContent: 'center' }}
                  size='large'
                  color="#fff"
                />
                <View style={Styles.MV2}>
                  <Text style={Styles.MVtext}>Please wait...</Text>
                </View>
              </View>
            </View>
          </Modal>
        </LinearGradient>
      </SafeAreaView>
    )
  }
  signUp = async () => {

    let apiUrl = global.ConstUrl + "users/signUp"

    if (!this.state.username == '' || !this.state.email == '' || !this.state.password == '' || !this.state.Confirmpass == '') {
      var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      if (!re.test(String(this.state.email).toLowerCase())) {
        return alert("Please Enter Valid Email")
      }
      if (this.state.password != this.state.Confirmpass) {
        return alert("Passwords don't match")
      } else {
        if (this.state.password.length < 8) {
          return alert("Passwords must be 8 character")
        }
      }

      this.setState({
        animate: true
      })

      let data = {
        name: this.state.username,
        email: this.state.email,
        password: this.state.password,
      }
      const userDetail = JSON.stringify(data)
      fetch(apiUrl, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: userDetail
      })
        .then(response =>
          response.json()).then(response => {
            console.log("SignUp", response)
            if (response.isSuccess) {
              this.setState({
                animate: false
              })
              this.SucessMessage()
            } else {
              alert(response.error)
              this.setState({
                animate: false
              })
            }
          })
        .catch(error => {
          console.log('Errorrrrrr:', error)
          alert("Please Check your internet Connection")
          this.setState({
            animate: false
          })
        })
    } else {
      Alert.alert('All fields are require')
    }

  }
}
