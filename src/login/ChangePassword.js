import React, { Component } from 'react'
import { Text, View, TextInput, SafeAreaView, ScrollView, Image, Button, TouchableOpacity } from 'react-native'
import Styles from '../../Stylesheet'
import LinearGradient from 'react-native-linear-gradient'

export default class ChangePassword extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
        <LinearGradient colors={global.ColorG} style={{flex:1}}>

                <ScrollView showsVerticalScrollIndicator={false}>
                    
                    <View style={Styles.ForgetPassLog}>
                    <View style={{height:30}}/>
                    
                        <Image source={require('../../img/assets/locked-padlock.png')} style={{ height: 50, width: 50, marginBottom: 40 }} />
                        <Text style={{ color: '#fff', fontSize: 25, fontWeight: 'bold' }}>CHANGE PASSWORD</Text>
                        <Text style={Styles.ForgetPassDis}>We just need to your registed Email-address to send you password reset.</Text>
                    </View>
                    <View style={{ flex: 1.5, }}>
                        <View style={{ marginLeft: 20, marginRight: 20 }}>
                            <View>
                                <Text style={Styles.LoginLabel}>OLD PASSWORD</Text>
                                <TextInput
                                    placeholder=''
                                    style={Styles.LoginText}
                                />
                            </View>
                            <View>
                                <Text style={Styles.LoginLabel}>NEW PASSWORD</Text>
                                <TextInput
                                    placeholder=''
                                    style={Styles.LoginText}
                                />
                            </View>
                            <View>
                                <Text style={Styles.LoginLabel}>CONFIRM PASSWORD</Text>
                                <TextInput
                                    placeholder=''
                                    style={Styles.LoginText}
                                />
                            </View>
                            <View >
                                <TouchableOpacity style={[Styles.ChangePassBtn, Styles.AJ]}>
                                    <Text style={Styles.LoginBtnTxt}>CREATE A PASSWORD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[Styles.LoginBtn, Styles.AJ]}>
                                    <Text style={Styles.LoginBtnTxt}>CANCEL</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ height: 30, }}/>
                    </View>

                </ScrollView>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}
