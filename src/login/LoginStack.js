import React, { Component } from 'react'
import { Text, View, } from 'react-native'
import { createStackNavigator,createAppContainer } from 'react-navigation'
import Login from './Login'
import SignUp from './SignUp'
import Splash from './Splash'
import ForgotPassword from './ForgotPassword'
import DrawerMenu from '../User/ProfileMenu'
import MainStack from '../MainStack'
import DrawerNavi from '../DrawerNavi';

const Stack=createStackNavigator({

  Login:{
    screen:Login,navigationOptions:{
      header:null
    }
  },
  SignUp:{
    screen:SignUp,navigationOptions:{
      header:null
    }
  },
  ForgotPassword:{
    screen:ForgotPassword,navigationOptions:{
      header:null
    }
  },
})

export default StackContainer=createAppContainer(Stack)
