import React, { Component } from 'react'
import { Text, View, SafeAreaView, TextInput, TouchableOpacity, Image,Alert,Modal,ActivityIndicator } from 'react-native'
import Styles from '../../Stylesheet'
import LinearGradient from 'react-native-linear-gradient'

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      
      email: '',
      borderWuser: 0,
      successmessage:'',
      animate: false,
   

    }
  }
  SucessMessage = () => {
    Alert.alert(
      "",
      this.state.successmessage,
      [
        { text: "OK", onPress: () => this.props.navigation.navigate('Login') },
      ],
      { cancelable: false },
    );
    return true;
 
  }
  _reset = async () => {
   
    email = this.state.email
    url = global.ConstUrl+'api/recover'
    var formdata = new FormData();

if (!email == '') {
  this.setState({
    animate: true,
    borderWuser:0,
  }) 
  formdata.append('email', email);
  fetch(url, {
    method: 'POST',
    headers: {
      'Authorization': global.BarCode
    },
    body: formdata
  })
  .then(response => response.json())
  .catch(error => {
    console.warn('Error:', error)
    this.setState({
        animate: false
      })
})
.then(response => {
  if (response.status =="success") {
    this.setState({
      animate: false
    })
    this.setState({successmessage:response.data})
    this.SucessMessage();
    console.warn(response)


  }
  else if(response.status =="error"){
    this.setState({
      animate: false
    })
    alert("Please enter valid e-mail address")
   

  }


})
  
} else {
  if (!email == '') {
    this.setState({ borderWuser: 0 })


} else {
    this.setState({ borderWuser: 2 })
}
  
}


  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
       <LinearGradient colors={global.ColorG} style={{flex:1}}>
        
         
        <View style={Styles.ForgetPassLog}>
          <Image source={require('../../img/assets/locked-padlock.png')} style={{ height: 50, width: 50, marginBottom: 40 }} />
          <Text style={{ color: '#fff', fontSize: 25, fontWeight: 'bold' }}>FORGOT PASSWORD</Text>
          <Text style={Styles.ForgetPassDis}>We just need to your registed Email-address to send you password reset.</Text>
        </View>
        <View style={{ flex:1.5,}}>
          <View style={{ margin:20}}>
            <View>
              <Text style={Styles.LoginLabel}>EMAIL ADDRESS</Text>
              <TextInput
                placeholder='Email address'
                onChangeText={(email) => this.setState({ email })}
                style={[Styles.LoginText, { borderWidth: this.state.borderWuser, borderColor: 'red' }]}
              />
            </View>
            <TouchableOpacity style={[Styles.LoginBtn, Styles.AJ]} onPress={this._reset}>
              <Text style={Styles.LoginBtnTxt}>
                RESET PASSWORD
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        </LinearGradient>
        <Modal visible={this.state.animate} transparent={true} >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                             }}>
                            <View style={Styles.ModalView}>
                                <ActivityIndicator
                                    animating={this.state.animate}
                                    // style={Styles.indicator}
                                    style={{justifyContent:'center'}}
                                    size='large'
                                    color="#fff"
                                />
                                <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                                </View>
                            </View>
                        </View>

                    </Modal> 
      </SafeAreaView>
    )
  }
}
